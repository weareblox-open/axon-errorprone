package com.blox.axon_errorprone;


import com.google.common.base.Predicates;
import com.google.errorprone.CompilationTestHelper;
import org.junit.jupiter.api.Test;

public class AggregateWithCommandHandlerOnConstructorTest {

    private final CompilationTestHelper compilationHelper = CompilationTestHelper.newInstance(AggregateWithCommandHandlerOnConstructor.class, getClass());

    @Test
    public void positiveCaseForAnnotationAggregate() {
        compilationHelper
                .expectErrorMessage("AggregateWithCommandHandlerOnConstructor", Predicates.containsPattern("AggregateWithCommandHandlerOnConstructor"))
                .addSourceFile("CommandHandlerOnConstructor_Positive_AggregateAnnotation.java")
                .doTest();
    }

    @Test
    public void positiveCaseForAnnotationAggregateRoot() {
        compilationHelper
                .expectErrorMessage("AggregateWithCommandHandlerOnConstructor", Predicates.containsPattern("AggregateWithCommandHandlerOnConstructor"))
                .addSourceFile("CommandHandlerOnConstructor_Positive_AggregateRootAnnotation.java")
                .doTest();
    }

    @Test
    public void positiveCaseForApplyStaticImport() {
        compilationHelper
                .expectErrorMessage("AggregateWithCommandHandlerOnConstructor", Predicates.containsPattern("AggregateWithCommandHandlerOnConstructor"))
                .addSourceFile("CommandHandlerOnConstructor_Positive_ApplyStaticImport.java")
                .doTest();
    }

    @Test
    public void positiveCaseForMultiLineApplyStaticImport() {
        compilationHelper
                .expectErrorMessage("AggregateWithCommandHandlerOnConstructor", Predicates.containsPattern("AggregateWithCommandHandlerOnConstructor"))
                .addSourceFile("CommandHandlerOnConstructor_Positive_MultiLineApplyStaticImport.java")
                .doTest();
    }

    @Test
    public void positiveCaseForAggregateLifeCycleApply() {
        compilationHelper
                .expectErrorMessage("AggregateWithCommandHandlerOnConstructor", Predicates.containsPattern("AggregateWithCommandHandlerOnConstructor"))
                .addSourceFile("CommandHandlerOnConstructor_Positive_AggregateLifeCycle_Apply.java")
                .doTest();
    }

    @Test
    public void positiveCaseForBothConstructorAndMethodCreationHandler() {
        compilationHelper
                .expectErrorMessage("AggregateWithCommandHandlerOnConstructor", Predicates.containsPattern("AggregateWithCommandHandlerOnConstructor"))
                .addSourceFile("CommandHandlerOnConstructor_Positive_BothConstructorAndMethodCreationHandler.java")
                .doTest();
    }

    @Test
    public void positiveCaseForAnnotationAggregateWithMultipleEventsApplied() {
        compilationHelper
                .expectErrorMessage("AggregateWithCommandHandlerOnConstructor", Predicates.containsPattern("AggregateWithCommandHandlerOnConstructor"))
                .addSourceFile("CommandHandlerOnConstructor_Positive_MultipleEventsApplied.java")
                .doTest();
    }

    @Test
    public void positiveCaseForAnnotationAggregateWithMetaData() {
        compilationHelper
                .expectErrorMessage("AggregateWithCommandHandlerOnConstructor", Predicates.containsPattern("AggregateWithCommandHandlerOnConstructor"))
                .addSourceFile("CommandHandlerOnConstructor_Positive_ApplyWithMetadata.java")
                .doTest();
    }

    @Test
    public void positiveCaseForMultipleConstructors() {
        compilationHelper
                .expectErrorMessage("AggregateWithCommandHandlerOnConstructor1", Predicates.containsPattern("AggregateWithCommandHandlerOnConstructor"))
                .expectErrorMessage("AggregateWithCommandHandlerOnConstructor2", Predicates.containsPattern("AggregateWithCommandHandlerOnConstructor"))
                .addSourceFile("CommandHandlerOnConstructor_Positive_MultipleConstructors.java")
                .doTest();
    }

    @Test
    public void negativeCaseForMethodHandlerWithCreationPolicy() {
        compilationHelper
                .addSourceFile("CommandHandlerOnConstructor_Negative_MethodHandlerWithCreationPolicy.java")
                .doTest();
    }

    @Test
    public void negativeCaseForMethodHandlerWithoutCreationPolicy() {
        compilationHelper
                .addSourceFile("CommandHandlerOnConstructor_Negative_MethodHandlerWithoutCreationPolicy.java")
                .doTest();
    }

    @Test
    public void negativeCaseForStateStoredAggregate() {
        compilationHelper
                .addSourceFile("CommandHandlerOnConstructor_Negative_StateStoredAggregate.java")
                .doTest();
    }

    @Test
    public void negativeCaseForAggregateWithNoApply() {
        compilationHelper
                .addSourceFile("CommandHandlerOnConstructor_Negative_NoApply.java")
                .doTest();
    }
}