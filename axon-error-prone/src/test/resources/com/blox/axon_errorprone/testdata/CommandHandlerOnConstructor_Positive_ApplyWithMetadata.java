package com.blox.axon_errorprone.testdata;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.messaging.MetaData;
import org.axonframework.modelling.command.AggregateRoot;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@AggregateRoot
class CommandHandlerOnConstructor_Positive_ApplyWithMetadata {

    @CommandHandler
    // BUG: Diagnostic matches: AggregateWithCommandHandlerOnConstructor
    public CommandHandlerOnConstructor_Positive_ApplyWithMetadata() {
        var metaData = MetaData.emptyInstance();
        apply(new Object(), metaData);
    }
}
