package com.blox.axon_errorprone.testdata;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.spring.stereotype.Aggregate;

import java.sql.Timestamp;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Aggregate
class CommandHandlerOnConstructor_Positive_MultipleConstructors {

    @CommandHandler
    // BUG: Diagnostic matches: AggregateWithCommandHandlerOnConstructor1
    public CommandHandlerOnConstructor_Positive_MultipleConstructors(Object command, Timestamp timestamp) {
        apply(new Object());
    }

    @CommandHandler
    // BUG: Diagnostic matches: AggregateWithCommandHandlerOnConstructor2
    public CommandHandlerOnConstructor_Positive_MultipleConstructors(Object command) {
        apply(new Object());
    }
}
