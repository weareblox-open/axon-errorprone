package com.blox.axon_errorprone.testdata;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.modelling.command.AggregateRoot;


@AggregateRoot
class CommandHandlerOnConstructor_Positive_AggregateLifeCycle_Apply {

    @AggregateIdentifier
    private String test;

    @CommandHandler
    // BUG: Diagnostic matches: AggregateWithCommandHandlerOnConstructor
    public CommandHandlerOnConstructor_Positive_AggregateLifeCycle_Apply() {
        AggregateLifecycle.apply(new Object());
    }
}
