package com.blox.axon_errorprone.testdata;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.modelling.command.AggregateCreationPolicy;
import org.axonframework.modelling.command.AggregateRoot;
import org.axonframework.modelling.command.CreationPolicy;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@AggregateRoot
class CommandHandlerOnConstructor_Negative_MethodHandlerWithCreationPolicy {

    public CommandHandlerOnConstructor_Negative_MethodHandlerWithCreationPolicy() {
    }

    @CommandHandler
    @CreationPolicy(AggregateCreationPolicy.CREATE_IF_MISSING)
    public void handle(Object object) {
        apply(new Object());
    }
}
