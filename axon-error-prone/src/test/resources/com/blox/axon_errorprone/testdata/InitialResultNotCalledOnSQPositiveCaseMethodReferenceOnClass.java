package com.blox.axon_errorprone.testdata;

import org.axonframework.queryhandling.QueryGateway;
import org.axonframework.queryhandling.SubscriptionQueryResult;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class InitialResultNotCalledOnSQPositiveCaseMethodReferenceOnClass {
    public InitialResultNotCalledOnSQPositiveCaseMethodReferenceOnClass(QueryGateway queryGateway){
        // test with correct and incorrect to make sure that incorrect Mono is not matching
        // the updates from there with th initialresult from the correct Mono.

        // TODO uncomment the correct piece and make the code pass the tests
        // CORRECT
//        Mono.defer(() -> Mono.just(queryGateway.subscriptionQuery(
//                new Object(), Void.class, Object.class)))
//            .doOnNext(SubscriptionQueryResult::initialResult)
//            .flatMapMany(SubscriptionQueryResult::updates);

        // INCORRECT
        Mono.defer(() -> Mono.just(queryGateway.subscriptionQuery(
                new Object(), Void.class, Object.class)))
            // BUG: Diagnostic matches: X
            .flatMapMany(SubscriptionQueryResult::updates);
    }
}
