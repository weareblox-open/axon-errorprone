package com.blox.axon_errorprone.testdata;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.modelling.command.AggregateCreationPolicy;
import org.axonframework.modelling.command.AggregateRoot;
import org.axonframework.modelling.command.CreationPolicy;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@AggregateRoot
class CommandHandlerOnConstructor_Positive_BothConstructorAndMethodCreationHandler {

    @CommandHandler
    // BUG: Diagnostic matches: AggregateWithCommandHandlerOnConstructor
    public CommandHandlerOnConstructor_Positive_BothConstructorAndMethodCreationHandler() {
        apply(new Object());
    }

    @CommandHandler
    @CreationPolicy(AggregateCreationPolicy.CREATE_IF_MISSING)
    public void handle(Object object) {
        apply(new Object());
    }
}
