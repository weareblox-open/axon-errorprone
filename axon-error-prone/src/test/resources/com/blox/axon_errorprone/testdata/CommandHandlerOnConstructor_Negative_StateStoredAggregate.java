package com.blox.axon_errorprone.testdata;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.modelling.command.AggregateCreationPolicy;
import org.axonframework.modelling.command.AggregateRoot;
import org.axonframework.modelling.command.CreationPolicy;

import javax.persistence.Entity;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Entity
class CommandHandlerOnConstructor_Negative_StateStoredAggregate {

    @CommandHandler
    public CommandHandlerOnConstructor_Negative_StateStoredAggregate() {
        apply(new Object());
    }
}
