package com.blox.axon_errorprone.testdata;

import org.axonframework.queryhandling.QueryGateway;
import org.axonframework.queryhandling.SubscriptionQueryResult;
import reactor.core.publisher.Flux;

public class InitialResultNotCalledOnSQPositiveCaseMethodInvocationOneCorrectOtherIncorrect {
    public InitialResultNotCalledOnSQPositiveCaseMethodInvocationOneCorrectOtherIncorrect(QueryGateway queryGateway){
        // correct SQ usage
        SubscriptionQueryResult<Void, Object> subscriptionQueryResult1 = queryGateway.subscriptionQuery(
            new Object(), Void.class, Object.class);
        subscriptionQueryResult1.initialResult();
        Flux.defer(() -> subscriptionQueryResult1.updates());
        // incorrect SQ usage
        SubscriptionQueryResult<Void, Object> subscriptionQueryResult2 = queryGateway.subscriptionQuery(
            new Object(), Void.class, Object.class);
            // BUG: Diagnostic matches: X
            Flux.defer(() -> subscriptionQueryResult2.updates());
    }
}
