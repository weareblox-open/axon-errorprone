package com.blox.axon_errorprone.testdata;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.modelling.command.AggregateCreationPolicy;
import org.axonframework.modelling.command.AggregateRoot;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@AggregateRoot
class CommandHandlerOnConstructor_Negative_MethodHandlerWithoutCreationPolicy {

    public CommandHandlerOnConstructor_Negative_MethodHandlerWithoutCreationPolicy() {
    }

    @CommandHandler
    public void handle(Object object) {
        apply(new Object());
    }
}
