package com.blox.axon_errorprone.testdata;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.spring.stereotype.Aggregate;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Aggregate
class CommandHandlerOnConstructor_Positive_AggregateAnnotation {

    @CommandHandler
    // BUG: Diagnostic matches: AggregateWithCommandHandlerOnConstructor
    public CommandHandlerOnConstructor_Positive_AggregateAnnotation() {
        apply(new Object());
    }
}
