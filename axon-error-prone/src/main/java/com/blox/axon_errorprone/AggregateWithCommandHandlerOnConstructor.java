package com.blox.axon_errorprone;

import com.google.auto.service.AutoService;
import com.google.errorprone.BugPattern;
import com.google.errorprone.VisitorState;
import com.google.errorprone.bugpatterns.BugChecker;
import com.google.errorprone.matchers.ChildMultiMatcher;
import com.google.errorprone.matchers.Description;
import com.google.errorprone.matchers.Matcher;
import com.google.errorprone.matchers.Matchers;
import com.sun.source.tree.ClassTree;
import com.sun.source.tree.ExpressionTree;
import com.sun.source.tree.MethodTree;

import static com.google.errorprone.BugPattern.SeverityLevel.WARNING;
import static com.google.errorprone.BugPattern.StandardTags.FRAGILE_CODE;

@BugPattern(
        name = "AggregateWithCommandHandlerOnConstructor",
        summary = "A @CommandHandler targeting a constructor can cause Aggregates to be recreated under certain conditions. Consider placing the @CommandHandler on a method together with a @CreationPolicy instead.",
        explanation = "Using a CommandHandler on a constructor can cause an Aggregate to be overwritten when the creation command is sent twice even though it shouldn't be allowed."
                + "The reason is that upon handling the command, Axon assumes that the resulting creation event will have a sequence number of 0."
                + "When Axon validates this assumption, it will not search the entire event store, but only the last 10 segments in order to improve performance."
                + "If the first creation event is not within those 10 segments, Axon will allow the sequence number and thereby also (partially) overwrite the original aggregate."
                + "By using a method with the @CreationPolicy annotation to create the Aggregate, Axon will forcefully check the repository to see if it can find an aggregate with the same id."
                + "This might not be a problem in all situations, and since the alternate approach might have performance implications, it is recommended to carefully consider the approach to use.",
        linkType = BugPattern.LinkType.CUSTOM,
        link = "https://gitlab.com/weareblox-open/axon-errorprone/-/wikis/AggregateWithCommandHandlerOnConstructor",
        severity = WARNING,
        tags = FRAGILE_CODE
)
@AutoService(BugChecker.class)
public class AggregateWithCommandHandlerOnConstructor extends BugChecker implements BugChecker.MethodTreeMatcher {

    public static final String AGGREGATE_ROOT_ANNOTATION = "org.axonframework.modelling.command.AggregateRoot";
    public static final String AGGREGATE_ANNOTATION = "org.axonframework.spring.stereotype.Aggregate";
    public static final String COMMAND_HANDLER_ANNOTATION = "org.axonframework.commandhandling.CommandHandler";

    private static final Matcher<ClassTree> IS_CLASS_WITH_AGGREGATE_ROOT_ANNOTATION =
            Matchers.annotations(
                    ChildMultiMatcher.MatchType.AT_LEAST_ONE,
                    Matchers.anyOf(
                            Matchers.isType(AGGREGATE_ROOT_ANNOTATION),
                            Matchers.isType(AGGREGATE_ANNOTATION)
                    )
            );

    private static final Matcher<MethodTree> IS_CONSTRUCTOR_WITH_COMMAND_HANDLER_ANNOTATION =
            Matchers.allOf(
                    Matchers.methodIsConstructor(),
                    Matchers.hasAnnotation(COMMAND_HANDLER_ANNOTATION)
            );

    private static final Matcher<ExpressionTree> APPLY_METHOD =
            Matchers.staticMethod()
                    .onClassAny("org.axonframework.modelling.command.AggregateLifecycle")
                    .named("apply");

    private static final Matcher<MethodTree> AGGREGATE_WITH_COMMAND_HANDLER_ON_CONSTRUCTOR_WHICH_APPLIES_EVENT =
            Matchers.allOf(
                    Matchers.enclosingClass(IS_CLASS_WITH_AGGREGATE_ROOT_ANNOTATION),
                    IS_CONSTRUCTOR_WITH_COMMAND_HANDLER_ANNOTATION,
                    Matchers.contains(ExpressionTree.class, Matchers.methodInvocation(APPLY_METHOD))
            );

    @Override
    public Description matchMethod(MethodTree tree, VisitorState state) {
        if (!AGGREGATE_WITH_COMMAND_HANDLER_ON_CONSTRUCTOR_WHICH_APPLIES_EVENT.matches(tree, state)) {
            return Description.NO_MATCH;
        }

        return describeMatch(tree);
    }
}
