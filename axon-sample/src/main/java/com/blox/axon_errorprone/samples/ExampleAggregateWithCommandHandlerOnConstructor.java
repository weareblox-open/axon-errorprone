package com.blox.axon_errorprone.samples;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.modelling.command.AggregateRoot;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@AggregateRoot
public class ExampleAggregateWithCommandHandlerOnConstructor {

    @CommandHandler
    // comment the SuppressWarnings-annotation when you want to see compilation warning
    @SuppressWarnings("AggregateWithCommandHandlerOnConstructor")
    public ExampleAggregateWithCommandHandlerOnConstructor() {
        apply(new Object());
    }

    @CommandHandler
    public void handle(Object command) {
        apply(new Object());
    }
}
