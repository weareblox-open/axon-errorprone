[![](https://jitpack.io/v/com.gitlab.weareblox-open/axon-errorprone.svg)](https://jitpack.io/#com.gitlab.weareblox-open/axon-errorprone)

# axon-errorprone
Compile-time checks for [Axon Framework](https://github.com/AxonFramework/AxonFramework) created as extension for [Google Error Prone](https://github.com/google/error-prone).

Running this plugin (together with error-prone) will help you preventing common errors while using Axon Framework by giving errors at compile time.

# Development

When opening in IntelliJ you might be greeted with errors such as `Package 'com.sun.tools.javac.code' is declared in module 'jdk.compiler', which does not export it to the unnamed module` You can safely apply the suggestion from IntelliJ to add the exports to the module compiler options. This is a problem in IntelliJ ( https://youtrack.jetbrains.com/issue/IDEA-154038 ) .
 
# Error Checks
## `InitialResultNotCalledOnSubscriptionQuery`
Method initialResult() is not called but updates() is called. For more info see; [wiki](https://gitlab.com/weareblox-open/axon-errorprone/-/wikis/InitialResultNotCalledOnSubscriptionQuery)


# Usage 
## Gradle
To use gradle with error prone you need the [Gradle ErrorProne Plugin](https://github.com/tbroyer/gradle-errorprone-plugin/)
```
plugins {
    id "net.ltgt.errorprone" version "2.0.2"
}

...

repositories {
    maven {
        url 'https://jitpack.io'
        content {              
            includeGroup 'com.gitlab.weareblox-open'
        }
    }
}
    
...

dependencies {
    errorprone 'com.google.errorprone:error_prone_core:2.10.0'
    errorprone 'com.gitlab.weareblox-open:axon-errorprone:0.0.9-SNAPSHOT'
}
```


## Maven
```
<repositories>
		<repository>
		    <id>jitpack.io</id>
		    <url>https://jitpack.io</url>
		</repository>
</repositories>
    
...

<dependency>
	    <groupId>com.gitlab.wearebloxp-open</groupId>
	    <artifactId>axon-errorprone</artifactId>
	    <version>0.0.9-SNAPSHOT</version>
</dependency>
```
## Release procedure

Imagine the current version is 0.0.7-SNAPSHOT, latest release was 0.0.6 and you want to release 0.0.7.
1. Change version number in root build.gradle to 0.0.7, then change version number in readme.md(two times).
2. Commit / push your code.
3. Tag your version, this can be done on Gitlab project page (probably also with command line but I haven’t tried that out). The name should be just the version number `0.0.7` , branch is the one you committed one. It’s not strictly necessary to add information to message or release notes.
4. Go back to the project page and click “Releases”.
    1. Then click “New release”.
    2. Select the just created tag
    3. Add info to title and release notes
    4. Click `Create release`
5. Go to https://jitpack.io/#com.gitlab.weareblox-open.axon-errorprone/ , check if the new version is shown and also check the logs if anything unexpected showed up during build.
6. Change the version number in root build.gradle to 0.0.8-SNAPSHOT and commit this.

Note;
Jitpack only builds tags and main so please be aware that when you’re referencing then please use `main-SNAPSHOT` as version OR use the commit hash as version ( can also be found in jitpack )
